/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.app;

import com.mycompany.spark.routes.Routes;
import com.mycompany.spark.util.Constant;
import com.mycompany.spark.util.Filters;
import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;

/**
 *
 * @author hoangnh
 */
public class Application {
    
    public static void main(String[] args) {
        port(Constant.Host.PORT);
        
        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Request-Method", "GET, POST, DELETE, PUT");
            response.header("Access-Control-Allow-Headers", "Content-Type");
            response.type("application/xml");
        });
        
        get("*", Constant.Host.CONTENT_TYPE_JSON, (req, res) -> { return Routes.getRoutes(req, res);});
        post("*", Constant.Host.CONTENT_TYPE_JSON, (req, res) -> { return Routes.getRoutes(req, res);});
//        post(Path.PROFILE_DETAIL, Constant.Host.CONTENT_TYPE_JSON, (req, res) -> { return DumpData.getProfile(req, res);});
        
        after("*", Filters.addGzipHeader);
    }
}
