/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class ChangeMailResponse extends ResponseData{
    private String owner_mail;
    private String change_mail;
    private Integer service_pt;

    public String getOwner_mail() {
        return owner_mail;
    }

    public void setOwner_mail(String owner_mail) {
        this.owner_mail = owner_mail;
    }

    public String getChange_mail() {
        return change_mail;
    }

    public void setChange_mail(String change_mail) {
        this.change_mail = change_mail;
    }

    public Integer getService_pt() {
        return service_pt;
    }

    public void setService_pt(Integer service_pt) {
        this.service_pt = service_pt;
    }
    
}
