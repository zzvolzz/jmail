/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class ListFootprintResponse extends ResponseData{
    private Integer last_foot_mark_id;

    public Integer getLast_foot_mark_id() {
        return last_foot_mark_id;
    }

    public void setLast_foot_mark_id(Integer last_foot_mark_id) {
        this.last_foot_mark_id = last_foot_mark_id;
    }
}
