/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class ListProfileResponse extends ResponseData{
    private Integer last_pf_id;
    private String last_login_time;
    private Integer corner_id;
    private String state;

    public Integer getLast_pf_id() {
        return last_pf_id;
    }

    public void setLast_pf_id(Integer last_pf_id) {
        this.last_pf_id = last_pf_id;
    }

    public String getLast_login_time() {
        return last_login_time;
    }

    public void setLast_login_time(String last_login_time) {
        this.last_login_time = last_login_time;
    }

    public Integer getCorner_id() {
        return corner_id;
    }

    public void setCorner_id(Integer corner_id) {
        this.corner_id = corner_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    

}
