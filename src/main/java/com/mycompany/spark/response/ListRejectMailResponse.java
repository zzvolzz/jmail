/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class ListRejectMailResponse extends ResponseData{
    private Integer last_reject_id;

    public Integer getLast_reject_id() {
        return last_reject_id;
    }

    public void setLast_reject_id(Integer last_reject_id) {
        this.last_reject_id = last_reject_id;
    }
}
