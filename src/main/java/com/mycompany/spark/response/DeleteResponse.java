/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class DeleteResponse extends ResponseData{
    private Integer delete_ct;

    public Integer getDelete_ct() {
        return delete_ct;
    }

    public void setDelete_ct(Integer delete_ct) {
        this.delete_ct = delete_ct;
    }
}
