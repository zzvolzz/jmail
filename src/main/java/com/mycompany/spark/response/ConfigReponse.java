/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class ConfigReponse extends ResponseData{
    private Integer push_notice_mail;
    private Integer push_notice_footmark;
    private Integer push_notice_favorite;
    private Integer footmark;
    private Integer pf_search;
    private Integer mail_flg;
    private Integer push_notice_dm;
    private String unique_id;

    public Integer getPush_notice_mail() {
        return push_notice_mail;
    }

    public void setPush_notice_mail(Integer push_notice_mail) {
        this.push_notice_mail = push_notice_mail;
    }

    public Integer getPush_notice_footmark() {
        return push_notice_footmark;
    }

    public void setPush_notice_footmark(Integer push_notice_footmark) {
        this.push_notice_footmark = push_notice_footmark;
    }

    public Integer getPush_notice_favorite() {
        return push_notice_favorite;
    }

    public void setPush_notice_favorite(Integer push_notice_favorite) {
        this.push_notice_favorite = push_notice_favorite;
    }

    public Integer getFootmark() {
        return footmark;
    }

    public void setFootmark(Integer footmark) {
        this.footmark = footmark;
    }

    public Integer getPf_search() {
        return pf_search;
    }

    public void setPf_search(Integer pf_search) {
        this.pf_search = pf_search;
    }

    public Integer getMail_flg() {
        return mail_flg;
    }

    public void setMail_flg(Integer mail_flg) {
        this.mail_flg = mail_flg;
    }

    public Integer getPush_notice_dm() {
        return push_notice_dm;
    }

    public void setPush_notice_dm(Integer push_notice_dm) {
        this.push_notice_dm = push_notice_dm;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }
}
