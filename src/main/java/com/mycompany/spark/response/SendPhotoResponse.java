/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class SendPhotoResponse extends ResponseData{
    private Integer photo_id;

    public Integer getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(Integer photo_id) {
        this.photo_id = photo_id;
    }
}
