/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class SendMailResponse extends ResponseData{
    private Integer mail_talk_id;

    public Integer getMail_talk_id() {
        return mail_talk_id;
    }

    public void setMail_talk_id(Integer mail_talk_id) {
        this.mail_talk_id = mail_talk_id;
    }
}
