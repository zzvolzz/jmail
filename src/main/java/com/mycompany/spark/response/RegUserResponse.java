/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class RegUserResponse extends ResponseData{
    private String sid;
    private String unique_id;
    private Integer usrid;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public Integer getUsrid() {
        return usrid;
    }

    public void setUsrid(Integer usrid) {
        this.usrid = usrid;
    }
}
