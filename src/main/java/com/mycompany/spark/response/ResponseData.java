/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.response;

import com.mycompany.spark.entity.Data;
import com.mycompany.spark.entity.DetailProfile;
import java.util.List;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "response")
public class ResponseData {
    private Integer status;
    
    private List<Data> items;
    
    private List<Data> arrays;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    @XmlElementWrapper
    @XmlAnyElement(lax=true)
    public List<Data> getItems() {
        return items;
    }

    public void setItems(List<Data> items) {
        this.items = items;
    }

    @XmlElementWrapper
    @XmlAnyElement(lax=true)
    public List<Data> getArrays() {
        return arrays;
    }

    public void setArrays(List<Data> arrays) {
        this.arrays = arrays;
    }
}
