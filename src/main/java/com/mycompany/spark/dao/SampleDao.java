/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.dao;

import com.mongodb.client.MongoCollection;
import com.mycompany.spark.util.Mongodb;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.Document;

/**
 *
 * @author hoangnh
 */
public class SampleDao {
    
    public static Boolean createUser(){
        Boolean status = false;
        
        MongoCollection<Document> account = Mongodb.getAccountCollection();
        
        Document doc = new Document();
        doc.append("userName", "user");
        doc.append("loginId", "user");
        doc.append("password", "xxxx");
        doc.append("email", "sss@gmail.com");
        doc.append("phone", 12345);
        
        try {
            account.insertOne(doc);
            status = true;
        } catch (Exception e) {
            Logger.getLogger("Create User").log(Level.SEVERE, null, e);
        }
        
        return status;
    }
    
    public static Document getUserInfo(String userId){
        Document data = new Document();
        
        MongoCollection<Document> account = Mongodb.getAccountCollection();
        
        Document doc = new Document();
        doc.append("loginId", userId);
        
        Document field = new Document();
        field.append("loginId", "1");
        field.append("userName", "1");
        field.append("email", "1");
        field.append("phone", "1");
        field.append("age", "1");
        field.append("height", "1");
        
        try {
            data = account.find(doc).projection(field).first();
        } catch (Exception e) {
            Logger.getLogger("Get User Info").log(Level.SEVERE, null, e);
        }
        return data;
    }
}
