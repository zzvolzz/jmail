/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "item")
public class MyProfile implements Data {
    private Integer now_pt;
    private Integer now_ticket_read;
    private Integer now_limited_ticket_read;
    private Integer now_ticket_res;
    private Integer now_limited_ticket_res;
    private Integer adult;
    private Integer prof_review;
    private String sex;
    private String nickname;
    private String state;
    private String city;
    private String pf_age;
    private String pf_oc;
    private String pf_bl;
    private String pf_he;
    private String pf_st;
    private String pf_lo;
    private String pf_ch;
    private String pf_pu;
    private String pf_car;
    private String self_pr;
    private String photo_url;

    public Integer getNow_pt() {
        return now_pt;
    }

    public void setNow_pt(Integer now_pt) {
        this.now_pt = now_pt;
    }

    public Integer getNow_ticket_read() {
        return now_ticket_read;
    }

    public void setNow_ticket_read(Integer now_ticket_read) {
        this.now_ticket_read = now_ticket_read;
    }

    public Integer getNow_limited_ticket_read() {
        return now_limited_ticket_read;
    }

    public void setNow_limited_ticket_read(Integer now_limited_ticket_read) {
        this.now_limited_ticket_read = now_limited_ticket_read;
    }

    public Integer getNow_ticket_res() {
        return now_ticket_res;
    }

    public void setNow_ticket_res(Integer now_ticket_res) {
        this.now_ticket_res = now_ticket_res;
    }

    public Integer getNow_limited_ticket_res() {
        return now_limited_ticket_res;
    }

    public void setNow_limited_ticket_res(Integer now_limited_ticket_res) {
        this.now_limited_ticket_res = now_limited_ticket_res;
    }

    public Integer getAdult() {
        return adult;
    }

    public void setAdult(Integer adult) {
        this.adult = adult;
    }

    public Integer getProf_review() {
        return prof_review;
    }

    public void setProf_review(Integer prof_review) {
        this.prof_review = prof_review;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPf_age() {
        return pf_age;
    }

    public void setPf_age(String pf_age) {
        this.pf_age = pf_age;
    }

    public String getPf_oc() {
        return pf_oc;
    }

    public void setPf_oc(String pf_oc) {
        this.pf_oc = pf_oc;
    }

    public String getPf_bl() {
        return pf_bl;
    }

    public void setPf_bl(String pf_bl) {
        this.pf_bl = pf_bl;
    }

    public String getPf_he() {
        return pf_he;
    }

    public void setPf_he(String pf_he) {
        this.pf_he = pf_he;
    }

    public String getPf_st() {
        return pf_st;
    }

    public void setPf_st(String pf_st) {
        this.pf_st = pf_st;
    }

    public String getPf_lo() {
        return pf_lo;
    }

    public void setPf_lo(String pf_lo) {
        this.pf_lo = pf_lo;
    }

    public String getPf_ch() {
        return pf_ch;
    }

    public void setPf_ch(String pf_ch) {
        this.pf_ch = pf_ch;
    }

    public String getPf_pu() {
        return pf_pu;
    }

    public void setPf_pu(String pf_pu) {
        this.pf_pu = pf_pu;
    }

    public String getPf_car() {
        return pf_car;
    }

    public void setPf_car(String pf_car) {
        this.pf_car = pf_car;
    }

    public String getSelf_pr() {
        return self_pr;
    }

    public void setSelf_pr(String self_pr) {
        this.self_pr = self_pr;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }
}
