/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "item")
public class DetailProfile implements Data {

    private Integer pf_id;
    private Integer bbs_id;
    private Integer favorite_id;
    private Integer foot_mark_id;
    private Integer snapshot_prof_id;
    private Integer corner_id;

    private Integer photo_id;
    private Integer favorite_status;
    private Integer bbs_status;
    private Integer new_bbs_id;
    private Integer new_bbs_corner_id;

    private Integer bbs_photo_id;
    private Integer bbs_movie_photo_id;
    private Integer movie_photo_id;
    private Integer movie_photo_url;

    private String sex;
    private String nickname;
    private String state;
    private String city;
    private String pf_age;
    private String pf_oc;
    private String pf_bl;
    private String pf_he;
    private String pf_st;
    private String pf_lo;
    private String pf_ch;
    private String pf_pu;
    private String pf_car;
    private String self_pr;

    private String photo_size;
    private String photo_url;
    private String new_bbs_corner_name;
    private String new_bbs_state;
    private String new_bbs_city;
    private String subj;
    private String comment;
    private String time;
    private String movie_photo_size;

    public Integer getPf_id() {
        return pf_id;
    }

    public void setPf_id(Integer pf_id) {
        this.pf_id = pf_id;
    }

    public Integer getBbs_id() {
        return bbs_id;
    }

    public void setBbs_id(Integer bbs_id) {
        this.bbs_id = bbs_id;
    }

    public Integer getFavorite_id() {
        return favorite_id;
    }

    public void setFavorite_id(Integer favorite_id) {
        this.favorite_id = favorite_id;
    }

    public Integer getFoot_mark_id() {
        return foot_mark_id;
    }

    public void setFoot_mark_id(Integer foot_mark_id) {
        this.foot_mark_id = foot_mark_id;
    }

    public Integer getSnapshot_prof_id() {
        return snapshot_prof_id;
    }

    public void setSnapshot_prof_id(Integer snapshot_prof_id) {
        this.snapshot_prof_id = snapshot_prof_id;
    }

    public Integer getCorner_id() {
        return corner_id;
    }

    public void setCorner_id(Integer corner_id) {
        this.corner_id = corner_id;
    }

    public Integer getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(Integer photo_id) {
        this.photo_id = photo_id;
    }

    public Integer getFavorite_status() {
        return favorite_status;
    }

    public void setFavorite_status(Integer favorite_status) {
        this.favorite_status = favorite_status;
    }

    public Integer getBbs_status() {
        return bbs_status;
    }

    public void setBbs_status(Integer bbs_status) {
        this.bbs_status = bbs_status;
    }

    public Integer getNew_bbs_id() {
        return new_bbs_id;
    }

    public void setNew_bbs_id(Integer new_bbs_id) {
        this.new_bbs_id = new_bbs_id;
    }

    public Integer getNew_bbs_corner_id() {
        return new_bbs_corner_id;
    }

    public void setNew_bbs_corner_id(Integer new_bbs_corner_id) {
        this.new_bbs_corner_id = new_bbs_corner_id;
    }

    public Integer getBbs_photo_id() {
        return bbs_photo_id;
    }

    public void setBbs_photo_id(Integer bbs_photo_id) {
        this.bbs_photo_id = bbs_photo_id;
    }

    public Integer getBbs_movie_photo_id() {
        return bbs_movie_photo_id;
    }

    public void setBbs_movie_photo_id(Integer bbs_movie_photo_id) {
        this.bbs_movie_photo_id = bbs_movie_photo_id;
    }

    public Integer getMovie_photo_id() {
        return movie_photo_id;
    }

    public void setMovie_photo_id(Integer movie_photo_id) {
        this.movie_photo_id = movie_photo_id;
    }

    public Integer getMovie_photo_url() {
        return movie_photo_url;
    }

    public void setMovie_photo_url(Integer movie_photo_url) {
        this.movie_photo_url = movie_photo_url;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPf_age() {
        return pf_age;
    }

    public void setPf_age(String pf_age) {
        this.pf_age = pf_age;
    }

    public String getPf_oc() {
        return pf_oc;
    }

    public void setPf_oc(String pf_oc) {
        this.pf_oc = pf_oc;
    }

    public String getPf_bl() {
        return pf_bl;
    }

    public void setPf_bl(String pf_bl) {
        this.pf_bl = pf_bl;
    }

    public String getPf_he() {
        return pf_he;
    }

    public void setPf_he(String pf_he) {
        this.pf_he = pf_he;
    }

    public String getPf_st() {
        return pf_st;
    }

    public void setPf_st(String pf_st) {
        this.pf_st = pf_st;
    }

    public String getPf_lo() {
        return pf_lo;
    }

    public void setPf_lo(String pf_lo) {
        this.pf_lo = pf_lo;
    }

    public String getPf_ch() {
        return pf_ch;
    }

    public void setPf_ch(String pf_ch) {
        this.pf_ch = pf_ch;
    }

    public String getPf_pu() {
        return pf_pu;
    }

    public void setPf_pu(String pf_pu) {
        this.pf_pu = pf_pu;
    }

    public String getPf_car() {
        return pf_car;
    }

    public void setPf_car(String pf_car) {
        this.pf_car = pf_car;
    }

    public String getSelf_pr() {
        return self_pr;
    }

    public void setSelf_pr(String self_pr) {
        this.self_pr = self_pr;
    }

    public String getPhoto_size() {
        return photo_size;
    }

    public void setPhoto_size(String photo_size) {
        this.photo_size = photo_size;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getNew_bbs_corner_name() {
        return new_bbs_corner_name;
    }

    public void setNew_bbs_corner_name(String new_bbs_corner_name) {
        this.new_bbs_corner_name = new_bbs_corner_name;
    }

    public String getNew_bbs_state() {
        return new_bbs_state;
    }

    public void setNew_bbs_state(String new_bbs_state) {
        this.new_bbs_state = new_bbs_state;
    }

    public String getNew_bbs_city() {
        return new_bbs_city;
    }

    public void setNew_bbs_city(String new_bbs_city) {
        this.new_bbs_city = new_bbs_city;
    }

    public String getSubj() {
        return subj;
    }

    public void setSubj(String subj) {
        this.subj = subj;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMovie_photo_size() {
        return movie_photo_size;
    }

    public void setMovie_photo_size(String movie_photo_size) {
        this.movie_photo_size = movie_photo_size;
    }

}
