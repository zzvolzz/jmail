/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.entity;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "array")
public class ArrayItem implements Data{
    
    private String array_name;
    private List<String> array_item;

    public String getArray_name() {
        return array_name;
    }

    public void setArray_name(String array_name) {
        this.array_name = array_name;
    }

    public List<String> getArray_item() {
        return array_item;
    }

    public void setArray_item(List<String> array_item) {
        this.array_item = array_item;
    }
}
