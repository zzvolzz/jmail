/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "item")
public class Mail implements Data{
    private Integer reject_id;
    private String sex;
    private Integer photo_id;
    private String photo_size;
    private String photo_url;
    private String nickname;
    private String state;
    private String pf_age;
    private String pf_oc;
    private String date;

    public Integer getReject_id() {
        return reject_id;
    }

    public void setReject_id(Integer reject_id) {
        this.reject_id = reject_id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(Integer photo_id) {
        this.photo_id = photo_id;
    }

    public String getPhoto_size() {
        return photo_size;
    }

    public void setPhoto_size(String photo_size) {
        this.photo_size = photo_size;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPf_age() {
        return pf_age;
    }

    public void setPf_age(String pf_age) {
        this.pf_age = pf_age;
    }

    public String getPf_oc() {
        return pf_oc;
    }

    public void setPf_oc(String pf_oc) {
        this.pf_oc = pf_oc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
