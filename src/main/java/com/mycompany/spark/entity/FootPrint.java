/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoangnh
 */
@XmlRootElement(name = "item")
public class FootPrint implements Data{
    private Integer foot_mark_id;
    private Integer corner_id;
    private String sex;
    private Integer photo_id;
    private String photo_size;
    private String photo_url;
    private String nickname;
    private String state;
    private String city;
    private String pf_age;
    private String pf_oc;
    private String date;
    private Integer status_flg;

    public Integer getFoot_mark_id() {
        return foot_mark_id;
    }

    public void setFoot_mark_id(Integer foot_mark_id) {
        this.foot_mark_id = foot_mark_id;
    }

    public Integer getCorner_id() {
        return corner_id;
    }

    public void setCorner_id(Integer corner_id) {
        this.corner_id = corner_id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(Integer photo_id) {
        this.photo_id = photo_id;
    }

    public String getPhoto_size() {
        return photo_size;
    }

    public void setPhoto_size(String photo_size) {
        this.photo_size = photo_size;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPf_age() {
        return pf_age;
    }

    public void setPf_age(String pf_age) {
        this.pf_age = pf_age;
    }

    public String getPf_oc() {
        return pf_oc;
    }

    public void setPf_oc(String pf_oc) {
        this.pf_oc = pf_oc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getStatus_flg() {
        return status_flg;
    }

    public void setStatus_flg(Integer status_flg) {
        this.status_flg = status_flg;
    }
}
