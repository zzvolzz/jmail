/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.util;

/**
 *
 * @author hoangnh
 */
public class Path {
    
    public static final String PROFILE_DETAIL = "/profile_detail/:userId";
    
    public static final String MAIL_READ_CHECK = "MailReadCheck";
    public static final String RESIGN_WARNING = "ResignWarning";
    public static final String PWD_CONFIRM = "PwdConfirm";
    public static final String ADVERTISE = "Advertise";
    public static final String POINT = "Point";
    public static final String FAVORITE_DELETE = "FavoriteDelete";
    public static final String FAVORITE_LIST = "FavoriteList";
    public static final String FAVORITE = "Favorite";
    public static final String LOGIN_INFO = "LoginInfo";
    public static final String MAIL_TALK_CHECK = "MailTalkCheck";
    public static final String CORNER = "Corner";
    public static final String NEW_CHECK = "NewCheck";
    public static final String INFO_CHECK = "InfoCheck";
    public static final String WRITE_DELETE = "WriteDelete";
    public static final String WRITE_HISTORY = "WriteHistory";
    public static final String WRITE = "Write";
    public static final String MAIL_EXCHANGE = "MailExchange";
    public static final String MAIL_LIST = "MailList";
    public static final String BBS_READ = "BBSRead";
    public static final String BBS_SEARCH_LIST = "BBSSearchList";
    public static final String MOVIE_SEND = "MovieSend";
    public static final String POINT_PURCHASE = "PointPurchase";
    public static final String PUSH_NOTICE = "PushNotice";
    public static final String REGI_START = "RegiStart";
    
    
}
