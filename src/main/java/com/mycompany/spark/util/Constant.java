/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.util;

/**
 *
 * @author hoangnh
 */
public class Constant {
    
    public static class Host{
        public static final Integer PORT = 4567;
        public static final String CONTENT_TYPE_JSON = "application/json";
    }
    
    public static class Database{
        public static final String HOST = "localhost";
        public static final Integer DB_PORT = 27017;
        
        public static final String DB_NAME = "chat_app";
        public static final String ACCOUNT = "account";
    }
    
    public static class Status{
        public static final Integer SUCCESS = 1;
        public static final Integer ERROR = 0;
    }
    
    public static class ErrorText{
        public static final String ERROR0 = "通信エラー が発生しました。時間を置いてアクセスしてください。";
        public static final String ERROR1 = "該当ありませんでした。";
        public static final String ERROR2 = "エラーが発生しました。もう一度やり直してください";
        public static final String ERROR3 = "ログインに失敗しました。再度ログインを行なってください。";
        public static final String ERROR4 = "ポイントが不足しています。";
        public static final String ERROR5 = "年齢 確認が必要です。このサービスのご利用には年齢 認証が必要です。";
        public static final String ERROR6 = "ブロックされています。このユーザーからブロックされているため、メールを送ることが出来ません。";
        public static final String ERROR7 = "通報内容を選択してください";
        public static final String ERROR8 = "コメントを入力してください";
        public static final String ERROR9 = "メールアドレス・電話番号・会員IDを入力してください";
        public static final String ERROR10 = "メールアドレスには半角英数字、又は「-」「_」「.」のみ使用できます";
        public static final String ERROR11 = "<@prmLoginIdType>がおかしいようです";
        public static final String ERROR12 = "パスワードを入力してください";
        public static final String ERROR13 = "パスワードには半角英数字、又は「-」「_」「.」のみ使用できます";
        public static final String ERROR14 = "既に退会しております。再登録を行なって下さい";
        public static final String ERROR15 = "お客様はサイトにそぐわない利用を行った為、利用禁止となっております";
        public static final String ERROR16 = "メールアドレスが違うか未登録です";
        public static final String ERROR17 = "携帯番号が違うか未登録です";
        public static final String ERROR18 = "パスワードが違うか未登録です";
        public static final String ERROR19 = "既に登録済みです。ログインを行なって下さい。";
        public static final String ERROR20 = "メールアドレスが確認できませんでした。「同意してメール送信」を押して空メールを送信してください";
        public static final String ERROR21 = "メールアドレスが既に登録済みです。ログインを行なって下さい。";
        public static final String ERROR22 = "携帯番号が既に登録済みです。ログインを行なって下さい。";
        public static final String ERROR23 = "登録されている会員IDが一致しました\\r\\n<@prmUsrUniqueId>\\r\\nログインを行なって下さい。";
        public static final String ERROR24 = "以前登録した性別と違っているようです。もう一度最初からやり直してください。";
        public static final String ERROR25 = "現在のパスワードが入力されていません";
        public static final String ERROR26 = "新しいパスワードが入力されていません";
        public static final String ERROR27 = "パスワードは4桁以上で入力して下さい";
        public static final String ERROR28 = "現在のパスワードが間違っています";
        public static final String ERROR29 = "更新に失敗しました。もう一度やり直してください";
        public static final String ERROR30 = "タイトルを入力してください。";
        public static final String ERROR31 = "件名を短く編集して下さい";
        public static final String ERROR32 = "本文を10文字以上で入力してください。";
        public static final String ERROR33 = "二重送信防止の為、送信間隔に規制を掛けています。送信の確認はﾒｰﾙBOXで行ってください";
        public static final String ERROR34 = "削除出来ませんでした。もう一度やり直してください";
        public static final String ERROR35 = "足あとの削除は行えません。自分の足あとのみ削除が行えます";
        public static final String ERROR36 = "ニックネームは必ず入力してください";
        public static final String ERROR37 = "ニックネーム が長すぎます。25文字までにしてください";
        public static final String ERROR38 = "年齢の項目では『指定なし』以外の項目を選択してください";
        public static final String ERROR39 = "プロフ変更に失敗しました。もう一度やり直してください";
        public static final String ERROR40 = "一部登録に失敗した可能性があります。事務局までお問い合わせください";
        public static final String ERROR41 = "性別 を選択して下さい。";
        public static final String ERROR42 = "都道府県 を選択してください。";
        public static final String ERROR43 = "年齢を選択してください。";
        public static final String ERROR44 = "お客様は画像利用禁止となっております";
        public static final String ERROR45 = "投稿は利用禁止となっております";
        public static final String ERROR46 = "二重投稿防止の為、投稿間隔に規制を掛けています。投稿の確認はﾒｰﾙBOXで行ってください";
        public static final String ERROR47 = "ファイルサイズが上限の30メガバイトを超えました";
        public static final String ERROR48 = "画像が添付されています。動画を送信してください";
        public static final String ERROR49 = "動画を認識できませんでした";
        public static final String ERROR50 = "<@prmNickname>さんは受信拒否中ですが、解除しますか？";
        public static final String ERROR51 = "お気に入り登録に失敗しました。もう一度やり直してください";
        public static final String ERROR52 = "お相手は退会されています。";
        public static final String ERROR53 = "本文が短すぎます。\\r\\n10文字以上で入力してください。";
        public static final String ERROR54 = "タイトルを入力してください。\\r\\n本文 を10文字以上で入力してください。";
        public static final String ERROR55 = "タイトルを入力してください。\\r\\n本文が短すぎます。\\r\\n10文字以上で入力してください。";
    }
}
