/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.util;

/**
 *
 * @author hoangnh
 */
public class Params {
    
    public static final String API_FLAG = "af";
    public static final String CONNER_ID = "cid";
    public static final String SESSION_ID = "sid";
    
    public static final String LAST_PROFILE = "lpfid";
    public static final String LAST_LOGIN = "llt";
    
    public static final String PHOTO_ID = "photoid";
    
    public static final String COMMENT = "comment";
    public static final String MOVIE_PHOTO_ID = "moviephotoid";
    
    public static final String FUNCTION_FLAG = "ff";
    public static final String DATA = "data";
    
    public static final String KIND = "kind";
    
    public static final String ITEM_REPORT = "br";
    
    public static final String LOGIN_ID = "loginid";
    public static final String PASSWORD = "pwd";
    public static final String OLD_PASSWORD = "nowpwd";
    public static final String NEW_PASSWORD = "newpwd";
    
    
    public static final String PROFILE_ID = "pfid";
    public static final String BROAD_ID = "bbsid";
    public static final String FAVORITE_ID = "fid";
    public static final String FOOTPRINT_ID = "fmid";
    public static final String MAIL_TOKEN = "mtid";
    public static final String SNAPSHOT_PROFILE = "spid";
     
    public static final String SEX = "sex";
    public static final String STATE = "state";
    public static final String CITY = "city";
    public static final String AGE1 = "age1";
    public static final String AGE2 = "age2";
    public static final String JOB = "oc";
    public static final String BLOOD = "bl";
    public static final String HEIGHT1 = "he1";
    public static final String HEIGHT2 = "he2";
    public static final String STYLE = "st";
    public static final String LOOKS = "lo";
    public static final String CHARACTER = "ch";
    public static final String HOBBY = "pu";
    public static final String CAR = "car";
    public static final String PHOTO = "photo";
    public static final String NAME = "name";
    public static final String WORD = "word";
    public static final String NEW = "new";
}
