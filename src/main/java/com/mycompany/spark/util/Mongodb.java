/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.util;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.Document;

/**
 *
 * @author hoangnh
 */
public class Mongodb {
    private static MongoClient client;
    
    public static MongoClient getClient(){
        try {
            if(client == null){
//                MongoClientURI uri = new MongoClientURI("");
//                client = new MongoClient(uri);
                client = new MongoClient(Constant.Database.HOST, Constant.Database.DB_PORT);
            }
        } catch (Exception e) {
            Logger.getLogger("Connect DB").log(Level.SEVERE, null, e);
        }
        return client;
    }
    
    public static MongoDatabase getDatabase(){
        MongoDatabase db = Mongodb.getClient().getDatabase(Constant.Database.DB_NAME);
        db.withWriteConcern(WriteConcern.ACKNOWLEDGED);
        return db;
    }
    
    public static MongoCollection<Document> getAccountCollection(){
        MongoCollection<Document> account = Mongodb.getDatabase().getCollection(Constant.Database.ACCOUNT);
        return account;
    }
}
