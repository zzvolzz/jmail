/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.util;

import com.mycompany.spark.entity.ArrayItem;
import com.mycompany.spark.response.ResponseData;
import com.mycompany.spark.entity.DetailProfile;
import com.mycompany.spark.entity.FootPrint;
import com.mycompany.spark.entity.Mail;
import com.mycompany.spark.entity.MyProfile;
import com.mycompany.spark.entity.Profile;
import com.mycompany.spark.response.ListProfileResponse;
import java.io.StringWriter;
import java.io.Writer;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

/**
 *
 * @author hoangnh
 */
public class Function {
    
    public static String buildXml(Object res){
        System.err.println("buildXml");
        String result = "";
        try{
//            JAXBContext context = JAXBContext.newInstance(ResponseData.class, ListProfileResponse.class, Profile.class);
            JAXBContext context = JAXBContext.newInstance(res.getClass(), DetailProfile.class, Profile.class, FootPrint.class, Mail.class, ArrayItem.class, MyProfile.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            
            Writer data = new StringWriter();
            m.marshal(res, data);
            result = data.toString();
        }catch(Exception e){
            System.err.println(e);
        }
        return result;
    }
}
