/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.controller;

import com.mycompany.spark.entity.ArrayItem;
import com.mycompany.spark.entity.DetailProfile;
import com.mycompany.spark.entity.Data;
import com.mycompany.spark.entity.FootPrint;
import com.mycompany.spark.entity.Mail;
import com.mycompany.spark.entity.MyProfile;
import com.mycompany.spark.entity.Profile;
import com.mycompany.spark.response.ChangeMailResponse;
import com.mycompany.spark.response.ConfigReponse;
import com.mycompany.spark.response.DeleteResponse;
import com.mycompany.spark.response.ErrorResponse;
import com.mycompany.spark.response.ImageResponse;
import com.mycompany.spark.response.ListFootprintResponse;
import com.mycompany.spark.response.ListProfileResponse;
import com.mycompany.spark.response.ListRejectMailResponse;
import com.mycompany.spark.response.LoginResponse;
import com.mycompany.spark.response.RegUserResponse;
import com.mycompany.spark.util.Function;
import com.mycompany.spark.util.Params;
import com.mycompany.spark.response.ResponseData;
import com.mycompany.spark.response.SendMailResponse;
import com.mycompany.spark.response.SendPhotoResponse;
import com.mycompany.spark.util.Constant;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import spark.Request;
import spark.Response;

/**
 *
 * @author hoangnh
 */
public class DumpData {

    public static String getProfile(Request req, Response res) {
        String connerId = req.queryParams(Params.CONNER_ID);
        String sessionId = req.queryParams(Params.SESSION_ID);

        String profile = req.queryParams(Params.PROFILE_ID);
        String broad = req.queryParams(Params.BROAD_ID);
        String favorite = req.queryParams(Params.FAVORITE_ID);
        String footprint = req.queryParams(Params.FOOTPRINT_ID);
        String mailToken = req.queryParams(Params.MAIL_TOKEN);
        String snapshotProfile = req.queryParams(Params.SNAPSHOT_PROFILE);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(0);
            data.setError_text(Constant.ErrorText.ERROR0);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }

        if (connerId == null || connerId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (connerId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(3);
            data.setError_text(Constant.ErrorText.ERROR3);
            return Function.buildXml(data);
        }

        DetailProfile pro = new DetailProfile();
        pro.setCorner_id(1);
        pro.setSex("男性");
        pro.setNickname("タロウ");
        pro.setState("福岡");
        pro.setCity("福岡市中央区");
        pro.setPf_age("30～34");
        pro.setPf_oc("会社員");
        pro.setPf_bl("A型");
        pro.setPf_he("150cm未満");
        pro.setPf_st("標準");
        pro.setPf_lo("さわやか系");
        pro.setPf_ch("やさしい");
        pro.setPf_pu("メル友/");
        pro.setPf_car("あり");
        pro.setSelf_pr("メールください");
        pro.setPhoto_id(199);
        pro.setPhoto_size("150");
        pro.setPhoto_url("http://○○.com/ms/mb/P.aspx?*******");
        pro.setFavorite_status(1);
        pro.setBbs_status(1);
        pro.setNew_bbs_id(1234);
        pro.setNew_bbs_corner_id(1234);
        pro.setNew_bbs_corner_name("ﾒﾙ友･ﾋﾟｭｱ恋");
        pro.setNew_bbs_state("福岡");
        pro.setNew_bbs_city("福岡市中央区");
        pro.setSubj("件名");
        pro.setComment("本文");
        pro.setTime("日時");
        pro.setBbs_photo_id(123);

        List<Data> list = new ArrayList<>();
        list.add(pro);

        ResponseData data = new ResponseData();
        data.setItems(list);
        data.setStatus(1);

        res.type("application/xml");
        return Function.buildXml(data);
    }

    public static String getProfileList(Request req, Response res) {
        String connerId = req.queryParams(Params.CONNER_ID);
        String sessionId = req.queryParams(Params.SESSION_ID);
        String lastProfile = req.queryParams(Params.LAST_PROFILE);
        String lastLogin = req.queryParams(Params.LAST_LOGIN);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(0);
            data.setError_text(Constant.ErrorText.ERROR0);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }

        if (lastProfile != null && lastProfile.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        }

        Profile pro = new Profile();
        pro.setPf_id(1234);
        pro.setPhoto_id(199);
        pro.setPhoto_size("150");
        pro.setPhoto_url("http://○○.com/ms/mb/P.aspx?*******");
        pro.setState("福岡");
        pro.setPf_age("26～29");
        pro.setSex("男性");
        pro.setLogin_time("2012/06/26 20:00:00");

        List<Data> list = new ArrayList<>();
        list.add(pro);
        list.add(pro);
        list.add(pro);

        ListProfileResponse data = new ListProfileResponse();
        data.setItems(list);
        data.setStatus(1);
        data.setLast_pf_id(2001);
        data.setLast_login_time("2012/06/26 20:00:00");
        data.setCorner_id(30);
        data.setState("福岡");
        return Function.buildXml(data);
    }

    public static String getExPhoto(Request req, Response res) {
        String connerId = req.queryParams(Params.CONNER_ID);
        String sessionId = req.queryParams(Params.SESSION_ID);
        String photoId = req.queryParams(Params.PHOTO_ID);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(0);
            data.setError_text(Constant.ErrorText.ERROR0);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }

        if (photoId != null && photoId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        }

        ImageResponse data = new ImageResponse();
        data.setStatus(1);
        data.setPhoto_id(1234);
        data.setPhoto_size("-1");
        data.setPhoto_url("http://○○.com/ms/mb/P.aspx?*******");
        return Function.buildXml(data);
    }

    public static String checkAge(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(5);
            data.setError_text(Constant.ErrorText.ERROR5);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }

        ResponseData data = new ResponseData();
        data.setStatus(1);
        return Function.buildXml(data);
    }

    public static String sendMail(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        String photoId = req.queryParams(Params.PHOTO_ID);
        String comment = req.queryParams(Params.COMMENT);
        String moviePhotoId = req.queryParams(Params.MOVIE_PHOTO_ID);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(0);
            data.setError_text(Constant.ErrorText.ERROR0);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }

        if (comment == null || comment.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(32);
            data.setError_text(Constant.ErrorText.ERROR32);
            return Function.buildXml(data);
        }

        if (photoId == null || photoId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        }

        SendMailResponse data = new SendMailResponse();
        data.setStatus(1);
        data.setMail_talk_id(12345);
        return Function.buildXml(data);
    }

    public static String sendPhoto(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        String functionFlag = req.queryParams(Params.FUNCTION_FLAG);
        String photoData = req.queryParams(Params.DATA);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(0);
            data.setError_text(Constant.ErrorText.ERROR0);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }

        SendPhotoResponse data = new SendPhotoResponse();
        data.setStatus(1);
        data.setPhoto_id(1234);
        return Function.buildXml(data);
    }

    public static String deleteMail(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        String mailToken = req.queryParams(Params.MAIL_TOKEN);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(0);
            data.setError_text(Constant.ErrorText.ERROR0);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }

        if (mailToken == null || mailToken.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(34);
            data.setError_text(Constant.ErrorText.ERROR34);
            return Function.buildXml(data);
        }

        DeleteResponse data = new DeleteResponse();
        data.setStatus(1);
        data.setDelete_ct(5);
        return Function.buildXml(data);
    }

    public static String regUser(Request req, Response res) {
        String userName = req.queryParams(Params.NAME);

        if (userName == null || userName.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(15);
            data.setError_text(Constant.ErrorText.ERROR15);
            return Function.buildXml(data);
        } else if (userName.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(19);
            data.setError_text(Constant.ErrorText.ERROR19);
            return Function.buildXml(data);
        }

        RegUserResponse data = new RegUserResponse();
        data.setStatus(1);
        data.setSid("sssssssssssssss");
        data.setUsrid(1234);
        data.setUnique_id("012345678");
        return Function.buildXml(data);
    }

    public static String listFootprint(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(0);
            data.setError_text(Constant.ErrorText.ERROR0);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }

        FootPrint item = new FootPrint();
        item.setFoot_mark_id(1234);
        item.setCorner_id(1);
        item.setSex("男性");
        item.setPhoto_id(1234);
        item.setPhoto_size("150");
        item.setPhoto_url("http://○○.com/ms/mb/P.aspx?*******");
        item.setNickname("たらお");
        item.setState("福岡");
        item.setCity("福岡市中央区");
        item.setPf_age("26～29");
        item.setPf_oc("OL");
        item.setDate("日時 / thời gian");
        item.setStatus_flg(1);

        List<Data> list = new ArrayList<>();
        list.add(item);
        list.add(item);

        ListFootprintResponse data = new ListFootprintResponse();
        data.setItems(list);
        data.setStatus(1);
        data.setLast_foot_mark_id(9999);

        return Function.buildXml(data);
    }

    public static String deleteFootprint(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        String footprintId = req.queryParams(Params.FOOTPRINT_ID);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(0);
            data.setError_text(Constant.ErrorText.ERROR0);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }

        if (footprintId == null || footprintId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(35);
            data.setError_text(Constant.ErrorText.ERROR35);
            return Function.buildXml(data);
        }

        DeleteResponse data = new DeleteResponse();
        data.setStatus(1);
        data.setDelete_ct(5);
        return Function.buildXml(data);
    }

    public static String listRejectMail(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(0);
            data.setError_text(Constant.ErrorText.ERROR0);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        Mail item = new Mail();
        item.setReject_id(1234);
        item.setSex("男性");
        item.setPhoto_id(1234);
        item.setPhoto_size("150");
        item.setPhoto_url("http://○○.com/ms/mb/P.aspx?*******");
        item.setNickname("たらお");
        item.setState("福岡");
        item.setPf_age("26～29");
        item.setPf_oc("OL");
        item.setDate("日時 / thời gian");
        
        Mail item2 = new Mail();
        item2.setReject_id(5678);
        item2.setSex("女性");
        item2.setPhoto_id(56789);
        item2.setPhoto_size("150");
        item2.setPhoto_url("http://○○.com/ms/mb/P.aspx?*******");
        item2.setNickname("わかめ");
        item2.setState("福岡");
        item2.setPf_age("26～29");
        item2.setPf_oc("OL");
        item2.setDate("日時 / thời gian");
        
        List<Data> list = new ArrayList<>();
        list.add(item);
        list.add(item2);

        ListRejectMailResponse data = new ListRejectMailResponse();
        data.setItems(list);
        data.setStatus(1);
        data.setLast_reject_id(9999);

        return Function.buildXml(data);
    }
    
    public static String checkRejectMail(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        String kind = req.queryParams(Params.KIND);

        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        if (kind == null || kind.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(6);
            data.setError_text(Constant.ErrorText.ERROR6);
            return Function.buildXml(data);
        }

        ResponseData data = new ResponseData();
        data.setStatus(1);
        return Function.buildXml(data);
    }
    
    public static String setRejectMail(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        ResponseData data = new ResponseData();
        data.setStatus(1);
        return Function.buildXml(data);
    }
    
    public static String reportUser(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        String itemReport = req.queryParams(Params.ITEM_REPORT);
        String comment = req.queryParams(Params.COMMENT);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        if (itemReport == null || itemReport.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(7);
            data.setError_text(Constant.ErrorText.ERROR7);
            return Function.buildXml(data);
        }
        
        if (comment == null || comment.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(8);
            data.setError_text(Constant.ErrorText.ERROR8);
            return Function.buildXml(data);
        }
        
        ResponseData data = new ResponseData();
        data.setStatus(1);
        return Function.buildXml(data);
    }
    
    public static String getBadReason(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        ArrayItem item = new ArrayItem();
        
        if(!sessionId.contains("android")){
            item.setArray_name("bad_reason");
            
            List<String> listItem = new ArrayList<String>();
            listItem.add("他ｻｲﾄへの誘導宣伝行為");
            listItem.add("ｱﾄﾞﾚｽ収集業者");
            listItem.add("違法物・犯罪行為");
            listItem.add("自殺・殺人などの犯罪予告");
            listItem.add("詐欺・脅迫");
            listItem.add("個人情報の掲載");
            listItem.add("勧誘・営利目的");

            item.setArray_item(listItem);
        }
        
        List<Data> list = new ArrayList<>();
        list.add(item);
        
        ResponseData data = new ResponseData();
        data.setStatus(1);
        data.setArrays(list);
        return Function.buildXml(data);
    }
    
    public static String login(Request req, Response res) {
        String loginId = req.queryParams(Params.LOGIN_ID);
        String pass = req.queryParams(Params.PASSWORD);
        
        if(loginId == null || loginId.equals("")){
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(9);
            data.setError_text(Constant.ErrorText.ERROR9);
            return Function.buildXml(data);
        }else if(loginId.length() > 20){
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(10);
            data.setError_text(Constant.ErrorText.ERROR10);
            return Function.buildXml(data);
        }else if(loginId.equals("error")){
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(11);
            data.setError_text(Constant.ErrorText.ERROR11);
            return Function.buildXml(data);
        }
        
        if(pass == null || pass.equals("")){
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(12);
            data.setError_text(Constant.ErrorText.ERROR12);
            return Function.buildXml(data);
        }else if(pass.length() > 20){
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(13);
            data.setError_text(Constant.ErrorText.ERROR13);
            return Function.buildXml(data);
        }
        
        LoginResponse data = new LoginResponse();
        data.setStatus(1);
        data.setSid("sssssssssssssss");
        data.setUsrid(1234);
        data.setUnique_id("012345678");
        return Function.buildXml(data);
    }
    
    public static String myProfile(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        MyProfile pro = new MyProfile();
        pro.setNow_pt(500);
        pro.setNow_ticket_read(100);
        pro.setNow_limited_ticket_read(40);
        pro.setNow_ticket_res(50);
        pro.setNow_limited_ticket_res(10);
        pro.setAdult(1);
        pro.setProf_review(1);
        
        pro.setSex("男性");
        pro.setNickname("タロウ");
        pro.setState("福岡");
        pro.setCity("福岡市中央区");
        pro.setPf_age("30～34");
        pro.setPf_oc("会社員");
        pro.setPf_bl("A型");
        pro.setPf_he("150cm未満");
        pro.setPf_st("標準");
        pro.setPf_lo("さわやか系");
        pro.setPf_ch("やさしい");
        pro.setPf_pu("メル友/");
        pro.setPf_car("あり");
        pro.setSelf_pr("メールください");
        pro.setPhoto_url("http://○○.com/ms/mb/P.aspx?*******");

        List<Data> list = new ArrayList<>();
        list.add(pro);

        ResponseData data = new ResponseData();
        data.setItems(list);
        data.setStatus(1);

        return Function.buildXml(data);
    }
    
    public static String changeMail(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        ChangeMailResponse data = new ChangeMailResponse();
        data.setStatus(1);
        data.setOwner_mail("testtesttestaaa@docomo.ne.jp");
        data.setChange_mail("adr-****@**.com");
        data.setService_pt(20);
        return Function.buildXml(data);
    }
    
    public static String changePassword(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        String oldPass = req.queryParams(Params.OLD_PASSWORD);
        String newPass = req.queryParams(Params.NEW_PASSWORD);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        if(oldPass == null || oldPass.equals("")){
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(25);
            data.setError_text(Constant.ErrorText.ERROR25);
            return Function.buildXml(data);
        }
        
        if(newPass == null || newPass.equals("")){
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(26);
            data.setError_text(Constant.ErrorText.ERROR26);
            return Function.buildXml(data);
        }else if(newPass.length() > 20){
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(13);
            data.setError_text(Constant.ErrorText.ERROR13);
            return Function.buildXml(data);
        }
        
        ResponseData data = new ResponseData();
        data.setStatus(1);

        return Function.buildXml(data);
    }
    
    public static String resigns(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        ResponseData data = new ResponseData();
        data.setStatus(1);

        return Function.buildXml(data);
    }
    
    public static String changeProfile(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        ResponseData data = new ResponseData();
        data.setStatus(1);

        return Function.buildXml(data);
    }
    
    public static String changeConfig(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        ResponseData data = new ResponseData();
        data.setStatus(1);

        return Function.buildXml(data);
    }
    
    public static String getConfig(Request req, Response res) {
        String sessionId = req.queryParams(Params.SESSION_ID);
        
        if (sessionId == null || sessionId.isEmpty()) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(1);
            data.setError_text(Constant.ErrorText.ERROR1);
            return Function.buildXml(data);
        } else if (sessionId.equals("error")) {
            ErrorResponse data = new ErrorResponse();
            data.setStatus(0);
            data.setError_code(2);
            data.setError_text(Constant.ErrorText.ERROR2);
            return Function.buildXml(data);
        }
        
        ConfigReponse data = new ConfigReponse();
        data.setStatus(1);
        data.setPush_notice_mail(1);
        data.setPush_notice_footmark(1);
        data.setPush_notice_favorite(1);
        data.setFootmark(0);
        data.setPf_search(1);
        data.setMail_flg(1);
        data.setUnique_id("012345678");
        
        return Function.buildXml(data);
    }
    
    public static String getFile(Request req, Response res) {
        System.out.print("====getFile===");
        String result = "";
        try {
            File fXmlFile = new File("test.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult streamResult = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, streamResult);

            result = writer.toString();
        } catch (Exception e) {
            System.err.println(e);
        }
        return result;
    }
}
