/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.controller;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import spark.Request;
import spark.Response;

/**
 *
 * @author GVN-S
 */
public class DumpData2 {
    public static String getFile (Request req, Response res, String af){
        if (af == null || af.isEmpty()) return "";
        String result = "";
        try{
            File fXmlFile = new File(af + ".xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);            
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult streamResult = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, streamResult);            
            result = writer.toString();
        }catch(IOException | ParserConfigurationException | TransformerException | SAXException e){
            System.err.println(e);
            result = "<?xml version=\"1.0\"?><response><status>0</status><error_code>0</error_code><error_text>通信エラー が発生しました。時間を置いてアクセスしてください。</error_text></response>";
        }
        res.type("application/xml");
        return result;
    }
}
