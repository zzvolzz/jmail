/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark.routes;

import com.mycompany.spark.controller.DumpData;
import com.mycompany.spark.controller.DumpData2;
import com.mycompany.spark.util.Params;
import com.mycompany.spark.util.Path;
import spark.Request;
import spark.Response;

/**
 *
 * @author hoangnh
 */
public class Routes {
    
    public static String getRoutes (Request req, Response res){
        String api = req.queryParams(Params.API_FLAG);
        System.err.println("Call api: "+api);
        
        String result = "";
        
        switch(api){
            case "ProfRead":
                result = DumpData.getProfile(req, res);
//                result = DumpData.getFile(req, res);
                break;
            case "ProfSearchList":
                result = DumpData.getProfileList(req, res);
                break;
            case "ExPhoto":
                result = DumpData.getExPhoto(req, res);
                break;
            case "AgeConfCheck":
                result = DumpData.checkAge(req, res);
                break;
            case "MailSend":
                result = DumpData.sendMail(req, res);
                break;
            case "PhotoSend":
                result = DumpData.sendPhoto(req, res);
                break;
            case "MailDelete":
                result = DumpData.deleteMail(req, res);
                break;
            case "UserRegi":
                result = DumpData.regUser(req, res);
                break;
            case "FootMarkList":
                result = DumpData.listFootprint(req, res);
                break;
            case "FootMarkDelete":
                result = DumpData.deleteFootprint(req, res);
                break;
            case "MailRejectList":
                result = DumpData.listRejectMail(req, res);
                break;
            case "MailRejectCheck":
                result = DumpData.checkRejectMail(req, res);
                break;
            case "MailRejectSet":
                result = DumpData.setRejectMail(req, res);
                break;
            case "BadUsrReport":
                result = DumpData.reportUser(req, res);
                break;
            case "BadReason":
                result = DumpData.getBadReason(req, res);
                break;
            case "Login":
                result = DumpData.login(req, res);
                break;
            case "MyProf":
                result = DumpData.myProfile(req, res);
                break;
            case "ChangeMail":
                result = DumpData.changeMail(req, res);
                break;
            case "PwdChange":
                result = DumpData.changePassword(req, res);
                break;
            case "Resigns":
                result = DumpData.resigns(req, res);
                break;
            case "ProfChange":
                result = DumpData.changeProfile(req, res);
                break;
            case "ConfigChange":
                result = DumpData.changeConfig(req, res);
                break;
            case "MyConfig":
                result = DumpData.getConfig(req, res);
                break;
                
                
            case Path.MAIL_READ_CHECK:
                result = DumpData2.getFile(req, res, Path.MAIL_READ_CHECK);
                break;
            case Path.RESIGN_WARNING:
                result = DumpData2.getFile(req, res, Path.RESIGN_WARNING);
                break;                
            case Path.PWD_CONFIRM:
                result = DumpData2.getFile(req, res, Path.PWD_CONFIRM);
                break;
            case Path.ADVERTISE:
                result = DumpData2.getFile(req, res, Path.ADVERTISE);
                break;
            case Path.POINT:
                result = DumpData2.getFile(req, res, Path.POINT);
                break;
            case Path.FAVORITE_DELETE:
                result = DumpData2.getFile(req, res, Path.FAVORITE_DELETE);
                break;
            case Path.FAVORITE_LIST:
                result = DumpData2.getFile(req, res, Path.FAVORITE_LIST);
                break;
            case Path.FAVORITE:
                result = DumpData2.getFile(req, res, Path.FAVORITE);
                break;
            case Path.LOGIN_INFO:
                result = DumpData2.getFile(req, res, Path.LOGIN_INFO);
                break;
            case Path.MAIL_TALK_CHECK:
                result = DumpData2.getFile(req, res, Path.MAIL_TALK_CHECK);
                break;
            case Path.CORNER:
                result = DumpData2.getFile(req, res, Path.CORNER);
                break;
            case Path.NEW_CHECK:
                result = DumpData2.getFile(req, res, Path.NEW_CHECK);
                break;
            case Path.INFO_CHECK:
                result = DumpData2.getFile(req, res, Path.INFO_CHECK);
                break;
            case Path.WRITE_DELETE:
                result = DumpData2.getFile(req, res, Path.WRITE_DELETE);
                break;
            case Path.WRITE_HISTORY:
                result = DumpData2.getFile(req, res, Path.WRITE_HISTORY);
                break;
            case Path.WRITE:
                result = DumpData2.getFile(req, res, Path.WRITE);
                break;
            case Path.MAIL_EXCHANGE:
                result = DumpData2.getFile(req, res, Path.MAIL_EXCHANGE);
                break;
            case Path.MAIL_LIST:
                result = DumpData2.getFile(req, res, Path.MAIL_LIST);
                break;
            case Path.BBS_READ:
                result = DumpData2.getFile(req, res, Path.BBS_READ);
                break;
            case Path.BBS_SEARCH_LIST:
                result = DumpData2.getFile(req, res, Path.BBS_SEARCH_LIST);
                break;
            case Path.MOVIE_SEND:
                result = DumpData2.getFile(req, res, Path.MOVIE_SEND);
                break;
            case Path.POINT_PURCHASE:
                result = DumpData2.getFile(req, res, Path.POINT_PURCHASE);
                break;
            case Path.PUSH_NOTICE:
                result = DumpData2.getFile(req, res, Path.PUSH_NOTICE);
                break;
            case Path.REGI_START:
                result = DumpData2.getFile(req, res, Path.REGI_START);
                break;
            default:
                result = "invalid api";
                break;
        }
        
        System.out.println("Result: "+result);
        return result;
    }
}
